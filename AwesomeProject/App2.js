/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>

        <View style={[styles.box1,styles.center]}>
    <View style={[styles.img,styles.center]}>
        <Text style={[styles.imgText,styles.center]}>Image</Text>
        </View>

        </View>

        <View style={[styles.box2,,styles.center]}>
        <View style={[styles.inputBox1,styles.center]}>
        <View style={styles.textInputBox}>
        <Text style={styles.textInput}>Input</Text>
        </View>
        <View style={styles.textInputBox}>
        <Text style={styles.textInput}>Input</Text>
        </View>
        </View>
        <View style={[styles.inputBox1,,styles.center]}>
        <View style={styles.textInputBox}>
        <Text style={styles.textInput}>Login</Text>
        </View>
        </View>
        

        </View>
      </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
  },
  header: {
    backgroundColor: 'white',
    alignItems: 'center',
  },
  headerText: {
    color: 'red',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 30,
  },
  content: {
    backgroundColor: 'blue',
    flex: 1,
    flexDirection: 'column'
  },
  box1: {
    backgroundColor: 'gray',
    flex: 1,
    
  },
  box2: {
    backgroundColor: 'gray',
    flex: 1,
    
  },
  center:{
    alignItems:'center',
    justifyContent:'center'
  },
  column: {
    backgroundColor: 'pink',
    flex: 1,
    flexDirection: 'column'
  },
  img:{
    backgroundColor: 'white',
    borderRadius:150,
    width:250,
    height:250,
  },
  imgText: {
    fontSize: 30,
    fontWeight: 'bold',
    padding: 40,
  },
  textInput: {
    fontSize: 30,
    fontWeight: 'bold',
    padding: 5,
    
  },
  textInputBox: {
    backgroundColor: 'pink',
    margin:10,
    alignItems: 'center',
    flexDirection: 'column',
    width: 300, 
    height: 60,
  },
  inputBox1:{
    flex: 1
  }
  
});
