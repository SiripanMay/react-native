/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (
            <View style={styles.container}>

                <View style={[styles.header, styles.center]}>
                    <View style={styles.boxIcon}>
                        <Text style={styles.icon}>=</Text>
                    </View>
                    <View style={[styles.box, styles.center]}>
                        <Text style={styles.icon}>Text</Text>
                    </View>
                    <View style={styles.boxIcon}>
                        <Text style={styles.icon}>X</Text>
                    </View>
                </View>

                <View style={[styles.content,styles.center]}>
                <Text style={styles.Text}>ScrollView</Text>
                </View>
                <View style={[styles.footer,styles.center]}>
                    <View style={styles.box}>
                        <Text style={styles.icon}>I</Text>
                    </View>
                    <View style={styles.box}>
                        <Text style={styles.icon}>C</Text>
                    </View>
                    <View style={styles.box}>
                        <Text style={styles.icon}>O</Text>
                    </View>
                    <View style={styles.box}>
                        <Text style={styles.icon}>N</Text>
                    </View>
                </View>

            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'cyan',
    },
    header: {
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal:-3
    },
    box: {
        backgroundColor: 'green',
        flex: 1,
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        backgroundColor: 'green',
        flex: 0,
        margin: 5,
    },
    footer: {
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal:-3
    },
    Text: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 30,
    },
    icon: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 10,
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        backgroundColor: 'cyan',
        flex: 1,
    },
    box1: {
        backgroundColor: 'gray',
        flex: 1,

    },
    box2: {
        backgroundColor: 'gray',
        flex: 1,

    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    column: {
        backgroundColor: 'pink',
        flex: 1,
        flexDirection: 'column'
    },


});
